use std::{fmt, io};

#[derive(Clone, Copy)]
struct Coordinates(i32, i32);

impl std::ops::Add for Coordinates {
    fn add(self, other: Coordinates) -> Coordinates {
        Coordinates(self.0 + other.0, self.1 + other.1)
    }
    type Output = Coordinates;
}

enum Direction {
    North,
    East,
    South,
    West,
}

impl fmt::Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::North => "North",
                Self::East => "East",
                Self::West => "West",
                Self::South => "South",
            }
        )
    }
}

pub struct Robot {
    position: Coordinates,
    direction: Direction,
}

impl fmt::Display for Robot {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Position: {}, {}\nDirection: {}",
            self.position.0, self.position.1, self.direction
        )
    }
}

impl Robot {
    pub fn send_instructions(&mut self, commands: &str) {
        let chars = commands.chars();
        for char in chars {
            if char == 'R' {
                self.direction = match self.direction {
                    Direction::North => Direction::East,
                    Direction::East => Direction::South,
                    Direction::South => Direction::West,
                    Direction::West => Direction::North,
                }
            }
            if char == 'L' {
                self.direction = match self.direction {
                    Direction::North => Direction::West,
                    Direction::West => Direction::South,
                    Direction::South => Direction::East,
                    Direction::East => Direction::North,
                }
            }
            if char == 'A' {
                self.position = match self.direction {
                    Direction::North => self.position + Coordinates(0, -1),
                    Direction::East => self.position + Coordinates(1, 0),
                    Direction::South => self.position + Coordinates(0, 1),
                    Direction::West => self.position + Coordinates(-1, 0),
                }
            }
        }
    }
    pub fn new() -> Self {
        Self {
            position: Coordinates(0, 0),
            direction: Direction::North,
        }
    }
}

fn main() {
    let mut robot = Robot::new();
    loop {
        println!("{}", robot);
        let mut buf = String::new();
        io::stdin().read_line(&mut buf).unwrap();
        robot.send_instructions(buf.as_str());
    }
}
